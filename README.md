Stock Rest Api
About
This is a rest application which deals with various operations on Stock. The application uses Spring Boot, so it is easy to run. This application make use of in memory H2 database. On startup, application will load some initial stock entries into the H2 database.
Considering the production environment application also provides HTTPS connector listening on port 8443, Also redirects all the http request on port 8080 to 8443

Application provides 3 Spring profiles
1.dev 
2.prod 
3.ft - for integration tests

What you’ll need
JDK 1.8

Maven 3.2+

You can also import the code straight into your IDE: Such as below, or anyone of your choice

Spring Tool Suite (STS)

IntelliJ IDEA

Getting started
Follow the steps to use the application. This will start app at the port 8443.

1. Download and unzip the source repository for this application, or clone it using Git: 

		git clone git@gitlab.com:pratik-shedge/stockrestapi.git

2. Go to the root directory of the cloned project using command prompt/terminal

3.  Running the project, choose any of the following

   * Run the `main` method from `StockRestApi`
   * Use the Maven Spring Boot plugin: `mvn spring-boot:run`

Access the application UI
Once the application is started, for 'dev' profile it creates Stock table as mentioned in schema.sql and inserts few stocks using ommandLineRunner bean  

UI can be accessed using the below URL:
		http://localhost:8080/api/viewstocks.html
   It will show a list of stocks, which can be updated.
   You can retrieve Stock details using search by Id
   You can also add a new stock
		
Swagger UI
To see the list of all operations supported by App and for API documentation, go to the following link.

http://localhost:8080/api/swagger-ui.html
Click on List operations and you will see the list of all supported operations. The list includes valid HTTP methods (GET, POST, PUT). Expanding each method provides additional useful data, such as response status, content-type, and a list of parameters.

It is also possible to try each method using the swagger UI.
REST endpoints
(1) Get All stocks.

     Method : GET 
     URL    : http://localhost:8080/api/stocks
(2) Get a stock by id.

     Method : GET 
     URL    : http://localhost:8080/api/stocks/{id}
(3) Create a stock.

     Method : POST
     URL    : http://localhost:8080/api/stocks
     Body   : { "name" : "Nestle", "currentPrice": "12.25"}
(4) Update an existing stock.

     Method : PUT
     URL    : http://localhost:8080/api/stocks/{id}
     Body   : { "currentPrice": "153.47"}