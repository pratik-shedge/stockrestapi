package com.payconiq.stock.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.verify;
import static org.mockito.Mockito.never;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.payconiq.stock.exception.NoSuchStockException;
import com.payconiq.stock.model.Stock;
import com.payconiq.stock.repository.StockRepository;
import com.payconiq.stock.view.StockView;

@RunWith(MockitoJUnitRunner.class)
public class StockServiceTest {
    @Mock
    private StockRepository stockRepository;

    @InjectMocks
    private StockService stockService;

    @Test
    public void get_list_of_stocks() {
        final List<Stock> stocks = new ArrayList<>();
        stocks.add(new Stock("Google", 125));
        stocks.add(new Stock("Apple", 135));

        Mockito.when(stockRepository.findAll()).thenReturn(stocks);
        List<StockView> stockList = stockService.getStockList();
        assertEquals(2, stockList.size());
        assertEquals("Google", stockList.get(0).getName());
        assertEquals("Apple", stockList.get(1).getName());
        assertEquals(125, stockList.get(0).getCurrentPrice().longValue());
        assertEquals(135, stockList.get(1).getCurrentPrice().longValue());
    }

    @Test
    public void get_empty_list_of_stocks() {
        final List<Stock> stocks = new ArrayList<>();
        Mockito.when(stockRepository.findAll()).thenReturn(stocks);
        List<StockView> stockList = stockService.getStockList();
        assertEquals(0, stockList.size());
    }

    @Test
    public void get_null_list_of_stocks() {
        Mockito.when(stockRepository.findAll()).thenReturn(null);
        List<StockView> stockList = stockService.getStockList();
        assertEquals(0, stockList.size());
    }

    @Test
    public void get_stock_by_id() {
        final Stock stock = new Stock("ING", 123);
        Mockito.when(stockRepository.findById(anyLong())).thenReturn(Optional.of(stock));
        StockView retrievedStock = stockService.getStock(1);
        assertEquals("ING", retrievedStock.getName());
        assertEquals(123, retrievedStock.getCurrentPrice().longValue());
    }

    @Test(expected = NoSuchStockException.class)
    public void get_stock_by_id_not_found() {
        Mockito.when(stockRepository.findById(anyLong())).thenReturn(Optional.empty());
        stockService.getStock(1);
    }

    @Test
    public void update_stock_by_id() {
        final StockView stock = new StockView("ING", 123);
        Mockito.when(stockRepository.findById(anyLong())).thenReturn(Optional.of(new Stock(stock)));
        stockService.updateStock(1, stock);
        verify(stockRepository).save(any(Stock.class));
    }

    @Test
    public void update_stock_by_id_not_found() {
        final StockView stock = new StockView("ING", 123);
        Mockito.when(stockRepository.findById(anyLong())).thenReturn(Optional.empty());
        try {
            stockService.updateStock(1, stock);
            fail();
        } catch (NoSuchStockException e) {
            verify(stockRepository, never()).save(any(Stock.class));
        }
    }

    @Test
    public void create_stock() {
        final StockView stock = new StockView("ING", 123);
        stockService.createStock(stock);
        verify(stockRepository).save(any(Stock.class));
    }

}