package com.payconiq.stock.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payconiq.stock.exception.NoSuchStockException;
import com.payconiq.stock.exception.StockAlreadyExistException;
import com.payconiq.stock.model.Stock;
import com.payconiq.stock.repository.StockRepository;
import com.payconiq.stock.service.StockService;
import com.payconiq.stock.view.StockView;

@RunWith(SpringRunner.class)
@WebMvcTest(StockController.class)
public class StockControllerTest {

    @MockBean
    private StockService stockService;

    @MockBean
    private StockRepository stockRepository;

    @Autowired
    private MockMvc mvc;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void givenStocks_thenReturnJsonArray_with_200() throws Exception {
        final List<StockView> stocks = new ArrayList<>();
        stocks.add(new StockView("Google", 125));
        stocks.add(new StockView("Apple", 135));

        Mockito.when(stockService.getStockList()).thenReturn(stocks);
        this.mvc.perform(get("/stocks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", equalTo(stocks.get(0).getName())))
                .andExpect(jsonPath("$[0].currentPrice", is(stocks.get(0).getCurrentPrice().doubleValue())))
                .andExpect(jsonPath("$[1].name", equalTo(stocks.get(1).getName())))
                .andExpect(jsonPath("$[1].currentPrice", is(stocks.get(1).getCurrentPrice().doubleValue())));
    }

    @Test
    public void givenStocksEmpty_thenReturn200() throws Exception {
        final List<StockView> stocks = new ArrayList<>();

        Mockito.when(stockService.getStockList()).thenReturn(stocks);
        this.mvc.perform(get("/stocks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void givenStockById_thenReturn_200_with_Stock() throws Exception {
        StockView stock = new StockView("Google", 125);
        Mockito.when(stockService.getStock(1)).thenReturn(stock);
        this.mvc.perform(get("/stocks/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.name", equalTo(stock.getName())))
                .andExpect(jsonPath("$.currentPrice", is(stock.getCurrentPrice().doubleValue())));
    }

    @Test
    public void givenStockById_Notfound_thenReturn_404() throws Exception {
        Mockito.when(stockService.getStock(1)).thenThrow(new NoSuchStockException());
        this.mvc.perform(get("/stocks/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenCreate_valid_stock_thenReturn_200() throws Exception {
        Stock stock = new Stock("Google", 125);
        this.mvc.perform(post("/stocks")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(objectMapper.writeValueAsString(stock)))
                .andExpect(status().isOk());
    }

    @Test
    public void givenUpdate_NotExistingstock_thenReturn_400() throws Exception {
        StockView stock = new StockView("Google", 125);
        Mockito.doThrow(new NoSuchStockException()).when(stockService).updateStock(1, stock);
        this.mvc.perform(put("/stocks/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenUpdate_Existingstock_thenReturn_200() throws Exception {
        Stock stock = new Stock("Google", 125);
        this.mvc.perform(put("/stocks/1")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(objectMapper.writeValueAsString(stock)))
                .andExpect(status().isOk());
    }
}
