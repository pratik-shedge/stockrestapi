package com.payconiq.stock;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payconiq.stock.model.Stock;
import com.payconiq.stock.repository.StockRepository;
import com.payconiq.stock.view.StockView;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application.yml")
public class StockApplicationIntegrationTests {

    @Autowired
    private StockRepository stockRepository;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private static final ObjectMapper mapper = new ObjectMapper();

    private static final HttpHeaders headers = new HttpHeaders();

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    @Test
    public void givenStocks_whenAllStocksInfoIsRetrieved_then200IsReceived() throws IOException {
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("api/stocks"),
                HttpMethod.GET, null, String.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        List<StockView> stocks = Arrays.asList(mapper.readValue(response.getBody(), StockView[].class));
        Assert.assertTrue(stocks.size() >= 5);
        Assert.assertEquals("Apple", stocks.get(0).getName());
        Assert.assertEquals(120, stocks.get(0).getCurrentPrice().longValue());
    }

    @Test
    public void givenSingleStock_byId_then200IsReceived() throws IOException {
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("api/stocks/2"),
                HttpMethod.GET, null, String.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        StockView stock = mapper.readValue(response.getBody(), StockView.class);
        Assert.assertEquals("Netflix", stock.getName());
        Assert.assertEquals(130, stock.getCurrentPrice().longValue());
    }

    @Test
    public void givenNonExistentStockId_then404IsReceived() {
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("api/stocks/12"),
                HttpMethod.GET, null, String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void givenExistingStock_whenCallUpdateStock_thenStockIsUpdated() {
        Stock stock = stockRepository.findById(5L).get();

        Assert.assertEquals("Facebook", stock.getName());
        Assert.assertEquals(160, stock.getCurrentPrice().longValue());

        //Updating the stock price to 2000
        StockView stockView = new StockView("Facebook", 2000);
        HttpEntity<StockView> entity = new HttpEntity<>(stockView, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("api/stocks/5"),
                HttpMethod.PUT, entity, String.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        Stock updatedStock = stockRepository.findById(5L).get();

        Assert.assertEquals("Facebook", updatedStock.getName());
        Assert.assertEquals(2000, updatedStock.getCurrentPrice().longValue());
    }

    @Test
    public void givenNonExistingStock_whenCallUpdateStock_thenBadRequest() {
        Optional<Stock> NonExistentStock = stockRepository.findById(125L);
        if (NonExistentStock.isPresent()) {
            Assert.fail();
        }

        //Updating the stock price to 2000
        StockView stockView = new StockView("ING", 2000);
        HttpEntity<StockView> entity = new HttpEntity<>(stockView, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("api/stocks/125"),
                HttpMethod.PUT, entity, String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void givenNonExistingStock_whenCallCreateStock_thenCreated() {
        Assert.assertFalse(stockRepository.findById(6L).isPresent());

        //creating the stock for Id : 100
        StockView stockView = new StockView("MicroSoft", 50);
        HttpEntity<StockView> entity = new HttpEntity<>(stockView, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("api/stocks/"),
                HttpMethod.POST, entity, String.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        Optional<Stock> createdStock = stockRepository.findById(6L);

        if (createdStock.isPresent()) {
            Assert.assertEquals("MicroSoft", createdStock.get().getName());
            Assert.assertEquals(50, createdStock.get().getCurrentPrice().longValue());
        } else {
            Assert.fail();
        }
    }

    @Test
    public void contextLoads() {
    }

}
