package com.payconiq.stock.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.payconiq.stock.model.Stock;
import com.payconiq.stock.service.StockService;
import com.payconiq.stock.view.StockView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for Stock Api.
 * Created by pratik.shedge on 2019-03-17.
 */

@RestController
@RequestMapping("/stocks")
@Api(tags = {"Stock Rest Api"})
public class StockController {
    private static final String PATH_ID = "{id}";
    private static final String PATH_VARIABLE_ID = "id";

    @Autowired
    private StockService stockService;

    /**
     * returns list of all stocks.
     *
     * @return list of stock
     */
    @GetMapping
    @ApiOperation(value = "provides list of all the available stocks", response = Stock.class, responseContainer = "list")
    public List<StockView> getAllStocks() {
        return stockService.getStockList();
    }


    /**
     * This Method returns a single stock by Id
     *
     * @param id stockId
     * @return {@link Stock}
     */
    @GetMapping(value = PATH_ID, produces = "application/json")
    @ApiOperation(value = "retrieve single stock based on id", response = Stock.class)
    public StockView getStock(@PathVariable(PATH_VARIABLE_ID) @NotNull Long id) {
        return stockService.getStock(id);
    }


    /**
     * This Method allows tp create a new stock in the Database
     *
     * @param stockView {@link Stock}
     */
    @PostMapping
    @ApiOperation(value = "creates a new stock into the store")
    public void createStock(@ApiParam(value = "Stock object", required = true) @Valid @RequestBody StockView stockView) {
        stockService.createStock(stockView);
    }


    /**
     * This method allows to update a stock in the database by Id
     *
     * @param id        stockId
     * @param stockView {@link Stock}
     */
    @PutMapping(PATH_ID)
    @ApiOperation(value = "updates the provided stock into the store")
    public void updateStock(@PathVariable(PATH_VARIABLE_ID) @NotNull long id, @Valid @RequestBody StockView stockView) {
        stockService.updateStock(id, stockView);
    }

}
