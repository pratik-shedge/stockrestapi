package com.payconiq.stock.view;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.payconiq.stock.model.Stock;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * StockView - view class for Stock entity.
 * Created by pratik.shedge on 2019-03-17.
 */

@Getter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StockView {

    private Long id;

    @NotBlank
    private String name;

    @NotNull
    private Double currentPrice;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime lastUpdate;

    public StockView(String name, double currentPrice) {
        this.name = name;
        this.currentPrice = currentPrice;
    }

    public StockView(@Valid Stock stock) {
        this.id = stock.getId();
        this.name = stock.getName();
        this.currentPrice = stock.getCurrentPrice();
        this.lastUpdate = stock.getLastUpdate();

    }
}
