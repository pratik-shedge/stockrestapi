package com.payconiq.stock.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.UpdateTimestamp;

import com.payconiq.stock.view.StockView;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Stock entity
 * Created by pratik.shedge on 2019-03-17.
 */

@ToString
@Getter
@NoArgsConstructor
@Entity
@Table(name = "STOCK")
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceGenerator")
    @SequenceGenerator(name = "SequenceGenerator", sequenceName = "STOCKIDSEQUENCE", allocationSize = 1)
    @Column(name = "ID")
    private Long id;

    @NotBlank
    @Setter
    @Column(name = "NAME")
    private String name;

    @NotNull
    @Setter
    @Column(name = "CURRENTPRICE")
    private Double currentPrice;

    @UpdateTimestamp
    @Column(name = "LASTUPDATE")
    private LocalDateTime lastUpdate;

    public Stock(String name, double currentPrice) {
        this.name = name;
        this.currentPrice = currentPrice;
    }

    public Stock(@Valid StockView stockView) {
        this.name = stockView.getName();
        this.currentPrice = stockView.getCurrentPrice();
    }
}
