package com.payconiq.stock.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payconiq.stock.exception.NoSuchStockException;
import com.payconiq.stock.model.Stock;
import com.payconiq.stock.repository.StockRepository;
import com.payconiq.stock.view.StockView;

/**
 * Stock service - Service layer for stock Application.
 * Created by pratik.shedge on 2019-03-17.
 */

@Service
public class StockService {

    @Autowired
    private StockRepository stockRepository;

    /**
     * returns list of all stocks from Database
     *
     * @return list of stocks
     */
    public List<StockView> getStockList() {
        List<Stock> stockList = stockRepository.findAll();
        if (Objects.isNull(stockList)) {
            return new ArrayList<>();
        }
        return stockList.stream().map(StockView::new).collect(Collectors.toList());
    }

    /**
     * This Method returns a single stock by Id
     *
     * @param id stockId
     * @return {@link StockView}
     */
    public StockView getStock(long id) {
        Optional<Stock> stock = stockRepository.findById(id);
        if (stock.isPresent()) {
            return new StockView(stock.get());
        }
        String message = String.format("Stock with id %s does not exist", id);
        throw new NoSuchStockException(message);
    }

    /**
     * This Method allows tp create a new stock in the Database
     *
     * @param stock {@link StockView}
     */
    public void createStock(StockView stock) {
        stockRepository.save(new Stock(stock));
    }

    /**
     * This method allows to update a stock in the database by Id
     *
     * @param id        stockId
     * @param stockView {@link StockView}
     */
    public void updateStock(long id, StockView stockView) {
        Optional<Stock> stock = stockRepository.findById(id);
        if (stock.isPresent()) {
            Stock stockUpdated = stock.get();
            stockUpdated.setName(stockView.getName());
            stockUpdated.setCurrentPrice(stockView.getCurrentPrice());
            stockRepository.save(stockUpdated);
        } else {
            String message = String.format("Stock with id %s does not exist", id);
            throw new NoSuchStockException(message);
        }

    }
}
