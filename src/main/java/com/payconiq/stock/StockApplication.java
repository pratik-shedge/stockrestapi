package com.payconiq.stock;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.payconiq.stock.model.Stock;
import com.payconiq.stock.repository.StockRepository;

/**
 * Main application class for Stock Application.
 * Created by pratik.shedge on 2019-03-17.
 */

@SpringBootApplication
public class StockApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockApplication.class, args);
    }

    @Bean
    @Profile("!prod")
    CommandLineRunner init(StockRepository stockRepository) {
        return args -> {
            stockRepository.save(new Stock("Apple", 120));
            stockRepository.save(new Stock("Netflix", 130));
            stockRepository.save(new Stock("Amazon", 140));
            stockRepository.save(new Stock("Google", 150));
            stockRepository.save(new Stock("Facebook", 160));
        };
    }

}
