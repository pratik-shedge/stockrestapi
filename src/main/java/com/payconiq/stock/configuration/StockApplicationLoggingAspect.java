package com.payconiq.stock.configuration;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Responsible for logging around of all the methods: logs method call details and method response details if method
 * completed without error. Logs also exception details when method completed with error.
 *
 * Created by pratik.shedge on 2019-03-17.
 */
@Slf4j
@Component
@Aspect
public class StockApplicationLoggingAspect {

	/**
	 * Define pointcut for this aspect: all methods in the the controller and service packages
	 * - are public
	 * - with any signature to denote any kind of method arguments)
	 */
	@Pointcut("execution(public * com.payconiq.stock.controller.*.*(..))"
			+ "execution(public * com.payconiq.stock.service.*.*(..))")
	public void allMethods() {
		// no body needed
	}

	/**
	 * Add logging to the functions of a resource
	 *
	 * @param pjp {@link ProceedingJoinPoint}
	 * @return the returns value as returned by the pointcut functions
	 * @throws Throwable rethrows what the pointcut functions throw
	 */
	@Around(value = "allMethods()")
	public  Object doAroundLogging(ProceedingJoinPoint pjp) throws Throwable {
		Object obj;
		try {
			this.logBeforeCall(pjp);
			Object ret = pjp.proceed();
			this.logAfterCall(ret, pjp);
			obj = ret;
		} catch (Throwable e) {
			this.logAfterThrowing(pjp, e);
			throw e;
		}

		return obj;
	}

	protected void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
		String message = this.afterThrowingMessage(joinPoint, e);
		log.error(message, e);
	}

    private String argsToString(JoinPoint pjp) {
		StringBuilder ret = new StringBuilder();
		Signature signature = pjp.getSignature();
		if (signature instanceof MethodSignature) {
			String[] paramNames = ((MethodSignature)pjp.getSignature()).getParameterNames();
			for(int i = 0; i < paramNames.length; ++i) {
				ret.append(pjp.getArgs()[i] != null ? pjp.getArgs()[i].toString() : "");
				if (i != paramNames.length - 1) {
					ret.append(", ");
				}
			}
		}

		return ret.toString();
	}

	protected void logBeforeCall(ProceedingJoinPoint joinPoint) {
		log.info(this.beforeCallMessage(joinPoint));
	}

	protected void logAfterCall(Object response, JoinPoint joinPoint) {
		String message = this.afterCallMessage(joinPoint, response);
		log.info(message);
	}

	private String beforeCallMessage(JoinPoint joinPoint) {
		return String.format("method %s >> arguments: %s", this.methodFullName(joinPoint), this.argsToString(joinPoint));
	}

    private String afterCallMessage(JoinPoint joinPoint, Object response) {
		return String.format("method %s << response: %s", this.methodFullName(joinPoint), response);
	}

    private String afterThrowingMessage(JoinPoint joinPoint, Throwable e) {
		return String.format("method %s <<< throws an exception: %s", this.methodFullName(joinPoint), e.getMessage());
	}

    private String methodFullName(JoinPoint joinPoint) {
		return joinPoint.getTarget().getClass().getSimpleName() + "#" + joinPoint.getSignature().getName();
	}

}