package com.payconiq.stock.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class StockAlreadyExistException  extends RuntimeException{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 1-param constructor for StockAlreadyExistException
     * @param msg error message
     */
    public StockAlreadyExistException(final String msg) {
        super(msg);
    }
}
