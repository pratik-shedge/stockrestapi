package com.payconiq.stock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.payconiq.stock.model.Stock;

/**
 * Stock repository contains all the DAO operations.
 * Created by pratik.shedge on 2019-03-17.
 */

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {
}
