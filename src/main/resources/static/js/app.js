var app = angular.module('app', []);

app.controller('StockCtrl', [
    '$scope',
    'StockService',
    function($scope, StockService) {
        $scope.onloadFun = function() {
            $scope.getAllStocks();
            $scope.stateChanged = false;
        }

        $scope.updateStock = function(stock) {
            StockService.updateStock(stock).then(
                function success(response) {
                    $scope.updateStockSuccess = 'Stock is updated successfully!';
                    $scope.updateStockError = '';
                    $scope.errorCode = '';

                    //Change the page state with the newly returned data
                    for(var i=0; i<$scope.stocks.length; i++){
                        if(response.data.id=== $scope.stocks[i].id){
                            $scope.stocks[i]=response.data;
                        }
                    }

                }, function error(response) {
                    $scope.updateStockSuccess = '';
                    $scope.errorCode = response.data.errorCode;
                    $scope.updateStockError = response.data.message;
                });

        }

        $scope.getStock = function(id) {
            $scope.shouldShowSearchTable = false;
            if(!id) {
                $scope.idEmpty = true;
            } else {
                $scope.idEmpty = false;
                StockService.getStock(id).then(
                    function success(response) {
                        $scope.shouldShowSearchTable = true;
                        $scope.stock = response.data;
                        $scope.message = '';
                        $scope.errorCode = '';
                    }, function error(response) {
                        $scope.message = response.data.message;
                        $scope.errorCode = response.data.errorCode;

                    });
            }
        }

        $scope.addStock = function(stock) {
            $scope.stateChanged = true;
            if (stock != null && stock.name && stock.currentPrice) {
                StockService.addStock(stock).then(
                    function success() {
                        $scope.stocks = $scope.getAllStocks();
                        $scope.addStockSuccess = 'Stock added successfully!';
                        $scope.addStockError = '';
                        $scope.errorMessage = '';
                    }, function error(response) {
                        $scope.addStockSuccess = '';
                        $scope.errorCode = response.data.errorCode
                        $scope.addStockError = response.data.message;
                    });
            }
        }

        $scope.getAllStocks = function() {
            StockService.getAllStocks().then(
                function success(response) {
                    $scope.stocks = response.data;
                    $scope.message = '';
                    $scope.errorCode = '';
                }, function error() {
                    $scope.message = '';
                    $scope.errorCode = 'Error getting stocks!';
                });
        }

    } ]);

app.service('StockService', [ '$http', function($http) {

    this.getStock = function getStock(stockId) {
        return $http({
            method : 'GET',
            url : 'stocks/' + stockId
        });
    }

    this.addStock = function addStock(stock) {
        return $http({
            method : 'POST',
            url : 'stocks/',
            data : {
                id : stock.id,
                name : stock.name,
                currentPrice : stock.currentPrice
            }
        });
    }

    this.updateStock = function updateStock(stock) {
        return $http({
            method : 'PUT',
            url : 'stocks/' + stock.id,
            data : {
                id : stock.id,
                name : stock.name,
                currentPrice : stock.currentPrice
            }
        })
    }

    this.getAllStocks = function getAllStocks() {
        return $http({
            method : 'GET',
            url : 'stocks'
        });
    }

} ]);